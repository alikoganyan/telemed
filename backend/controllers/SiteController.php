<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Feedback;
use common\models\FeedbackSearch;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
               'except' => ['login', 'error', 'index'],
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','delete','view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {


        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/login']);
        }


        $model = new Feedback();
        $searchModel = new FeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    public function actionDelete($id){

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/login']);
        }

        $model = Feedback::findOne(['id' => $id]);
        if($model){
            $model->delete();
        }
        return $this->redirect(['index']);
    }
    public function actionView($id){

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/login']);
        }

        $model = Feedback::findOne(['id'=>$id]);

        return $this->render('view',['model' => $model]);
    }
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/admin');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/login']);
        }

        Yii::$app->user->logout();

        return $this->redirect('/');
    }

}
